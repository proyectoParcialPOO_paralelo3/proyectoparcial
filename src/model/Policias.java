/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tablero;

/**
 *
 * @author boscoand
 */
public class Policias {
    
    private int pos_x;
    private int pos_y;
    private int vision;
    private String id = "P";
    
    public Policias(Tablero tab, int vision){
        this.vision = vision;
    }

    public int getPos_x() {
        return pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public int getVision() {
        return vision;
    }

    public String getId() {
        return id;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }
    
}
