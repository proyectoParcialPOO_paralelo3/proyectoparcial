/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.Tablero;

/**
 *
 * @author boscoand
 */
public class Agentes {

    //El límite es una constante para todo el universo y tiene un valor de 0.1
    private final double limite = 0.1;
    
    //El perjuicio es asignado aleatoriamente
    private double perjuicio;
    
    //El agravio depende del perjuicio
    private double agravio;
    
    //Cada miembro de la población también calcula un riesgo individual de rebelarse al comienzo de cada
    //turno. Esta PROBABILIDAD DE DETENCIÓN ESTIMADA se basa en el número de policías y
    //agente  s rebeldes dentro de los parches VISION, concretamente
    private double probabilidad_detencion;
    
    //AVERSION AL RIESGO es dado
    //aleatoriamente a cada uno de los agentes al momento de nacer (un valor del 0 al 1).
    private double aversion_riesgo;
    
    private double riesgo_neto;
    
    private boolean esRebelde = false;
    
    private int pos_x;
    private int pos_y;
    private int vision;
    private int agentes_rebelados_alrededor;
    private int policias_alrededor;
    
    private String id = "c";
    
    public Agentes(Tablero tab, int vision){
        this.vision = vision;
        generarPerjuicio();
        generarAgravio();
        generarAversion();
    }
    
    private void generarPerjuicio(){
        //this.agravio = (double) (Math.random() * 1) + 1;
        this.perjuicio = (double) (Math.random() * 1);
    }
    
    private void generarAgravio(){
        this.agravio = this.perjuicio * ( 1 - Gobierno.legitimidad);
    }
    
    public void generarProbabilidadDetencion(){
        //this.probabilidad_detencion = 1 - exp (-2.3 * round(C / A) )
        if(this.agentes_rebelados_alrededor == 0){
            this.probabilidad_detencion = 0;
            return;
        }
        this.probabilidad_detencion = 1 - Math.exp(-2.3 * Math.round(this.policias_alrededor/this.agentes_rebelados_alrededor));
    }
    
    private void generarAversion(){
        this.aversion_riesgo = (double) (Math.random() * 1);
    }
    
    public void generarRiesgoNeto(){
        this.riesgo_neto = this.aversion_riesgo * this.probabilidad_detencion;
    }
    
    public void comprobarRebeldia(){
        if((this.agravio - this.riesgo_neto) > 0.1){
            this.id = "R";
            this.esRebelde = true;
            return;
        }
        this.esRebelde = false;
    }
    
    public void contarPersonasAlrededor(Tablero tab){
        int min_x = pos_x - vision;
        int min_y = pos_y - vision;
        int max_x = pos_x + vision;
        int max_y = pos_y + vision;
        
        if(min_x < 0)
            min_x = 0;
        if(min_y < 0)
            min_y = 0;
        if(max_x > tab.getAlto()-1)
            max_x = tab.getAlto()-1;
        if(max_y >= tab.getAncho()-1)
            max_y = tab.getAncho()-1;
        
        //Se empieza en -1 para que no se añada al contador el agente que realiza el cálculo.
        this.agentes_rebelados_alrededor = 0;
        this.policias_alrededor = 0;
        for(int i=min_x; i<=max_x; i++){
            for(int j=min_y; j<=max_y; j++){    
                
                if(tab.getTab()[i][j] instanceof Agentes){
                    Agentes agente = (Agentes) tab.getTab()[i][j];
                    if(agente.isEsRebelde() && (agente != this))
                        //Hay que validar que solo se sumen agentes que se están revelando para poder hacer el cálculo 
                        this.agentes_rebelados_alrededor ++;
                    
                } else if(tab.getTab()[i][j] instanceof Policias){
                    this.policias_alrededor ++;
                }
            }
        }
        
        System.out.println("Pos" + pos_x + pos_y + " - Agentes" + agentes_rebelados_alrededor + " - Policias" + policias_alrededor);
        
    }

    public int getPos_x() {
        return pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public int getVision() {
        return vision;
    }

    public String getId() {
        return id;
    }

    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getProbabilidad_detencion() {
        return probabilidad_detencion;
    }

    public double getRiesgo_neto() {
        return riesgo_neto;
    }

    public boolean isEsRebelde() {
        return esRebelde;
    }

    public double getAgravio() {
        return agravio;
    }

    public void setEsRebelde(boolean esRebelde) {
        this.esRebelde = esRebelde;
    }

    public double getPerjuicio() {
        return perjuicio;
    }

    public double getAversion_riesgo() {
        return aversion_riesgo;
    }
    
    
}
