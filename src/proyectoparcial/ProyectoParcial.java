/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoparcial;

import java.util.ArrayList;
import java.util.Scanner;
import model.Agentes;
import model.Policias;
import view.Tablero;

/**
 *
 * @author MERCHAN-FERNANDEZ-MILLER
 */
public class ProyectoParcial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        /*
        System.out.println("-  Ingrese los siguientes datos: ");
        
        System.out.println("1) % de densidad Inicial de Policías: 0-100%: ");
        double densidad_policias = sc.nextDouble();
        
        System.out.println("2) % de densidad Inicial de Agentes (miembros de la población general) 0-100%");
        double densidad_poblacion = sc.nextDouble();
        
        System.out.println("Tamaño del Universo (Alto)");
        int tamaño_tablero_alto = sc.nextInt();
        
        System.out.println("Tamaño del Universo (Ancho)");
        int tamaño_tablero_ancho = sc.nextInt();
        
        System.out.println("Máximo numero de turnos en la cárcel");
        int turnos_carcel = sc.nextInt();
        
        System.out.println("Visión de policías y agentes");
        int vision = sc.nextInt();
        
        System.out.println("Máximo Número de turnos (Cuantas veces se correrá la simulación)");
        int turnos_simulacion = sc.nextInt();
        
        System.out.println("Legitimidad del Gobierno (Valor entre 0 y 1)");
        double legitimidad = sc.nextDouble();
        
        System.out.println("Movimiento: On o Off (Si esta apagado los miembros de la población no se mueven)");
        boolean movimiento = sc.nextBoolean();
        */
        double densidad_policias = 0;
        double densidad_poblacion = 0;
        int tamaño_tablero_alto = 7;
        int tamaño_tablero_ancho = 7;
        int turnos_carcel = 2;
        int vision_policias = 4;
        int vision_agentes = 2;
        int turnos_simulacion = 4;
        double legitimidad = 0.5;
        boolean movimiento = true;
        
        Tablero tab = new Tablero(tamaño_tablero_alto,tamaño_tablero_ancho);
        tab.imprimirTablero();
        
        //Prueba para iniciar con agente rebelandose
        /*for(int i=0 ; i<=2; i++){
            Agentes a_tmp = new Agentes(tab, vision_agentes);
            a_tmp.setId("C");
            a_tmp.setEsRebelde(true);
            tab.anadirObjeto(a_tmp);
        }*/
        //
        
        //Ingresa policias y agentes
        for(int i=0 ; i<=3; i++){
            System.out.println("\n\n");
            Policias pol = new Policias(tab, vision_policias);
            Agentes pob = new Agentes(tab, vision_agentes);
            tab.anadirObjeto(pob);
            tab.anadirObjeto(pol);
        }
        
        //Cuenta agentes alrededor
        for(int i=0 ; i<tamaño_tablero_alto; i++) {
            for(int j=0 ; j<tamaño_tablero_ancho; j++) {
                if(tab.getTab()[i][j] instanceof Agentes){
                    Agentes agente = (Agentes) tab.getTab()[i][j];
                    agente.contarPersonasAlrededor(tab);
                }    
            }
        }
        
        System.out.println("");
        tab.imprimirTablero();
         
        //Luego de poner a todos en el tablero se calcular la probabilidad de detencion
        for(int i=0 ; i<tamaño_tablero_alto; i++) {
            for(int j=0 ; j<tamaño_tablero_ancho; j++) {
                if(tab.getTab()[i][j] instanceof Agentes){
                    Agentes agente = (Agentes) tab.getTab()[i][j];
                    //if(agente.isEsRebelde()){
                        
                        agente.generarProbabilidadDetencion();
                        agente.generarRiesgoNeto();
                        agente.comprobarRebeldia();
                        
                        if(agente.isEsRebelde()){
                            System.out.println("El agente ubicado en: " + agente.getPos_x() + "," + agente.getPos_y() + " se ha rebelado. Su agravio - riesgo neto es: " + agente.getAgravio() + Math.round(agente.getRiesgo_neto()));
                        }
                        /*System.out.println("--Perjuicio (Aleatorio)  : " + agente.getPerjuicio());
                        
                        System.out.println("--Agravio legitimidad(0.2: perjuicio * ( 1 - legitimidad)");
                        System.out.println("--Agravio                : " + agente.getAgravio());
                        
                        System.out.println("--Aversion  (Aleatorio)  : " + agente.getAversion_riesgo());
                        System.out.println("--ProbDetencion          :" + agente.getProbabilidad_detencion());
                        
                        System.out.println("--Riesgo Neto            : aversion * probabilidad_detencion");
                        System.out.println("--Riesgo Neto            : " + agente.getRiesgo_neto());
                        
                        System.out.println("--Es Rebelde  : agravio - riesgo_neto > 0.1" + agente.isEsRebelde());
                        System.out.println("--Es Rebelde   " + agente.isEsRebelde());*/
                    //}
                }    
            }
        }
        
        System.out.println("");
        tab.imprimirTablero();
        System.out.println("- Policias con visión: " + vision_policias);
        System.out.println("- Agentes  con visión: " + vision_agentes);
    }
    
}
