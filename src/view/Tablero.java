/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import model.Agentes;
import model.Policias;

/**
 *
 * @author boscoand
 */
public class Tablero {

    private Object[][] tablero;
    private int alto;
    private int ancho;
    
    public Tablero(int alto, int ancho) {
        this.alto = alto;
        this.ancho = ancho;
        this.tablero = new Object[alto][ancho];
        for(int i=0 ; i<alto; i++) {
            for(int j=0 ; j<ancho; j++) {
                tablero[i][j] = null;
            }
        }
    }
    
    public void imprimirTablero(){
        
        for(int k=0 ; k<this.ancho; k++){
            System.out.print("   " + k);
        }
        System.out.println("\n");
        for(int i=0 ; i<this.alto; i++) {
            System.out.print(i + "  ");
            for(int j=0 ; j<this.ancho; j++) {
                if(tablero[i][j] instanceof Policias){
                    Policias p = (Policias)tablero[i][j];
                    System.out.print(p.getId()+"   ");
                }else if(tablero[i][j] instanceof Agentes){
                    Agentes a = (Agentes)tablero[i][j];
                    System.out.print(a.getId()+"   ");
                }else {
                    System.out.print("-   ");
                }
            }
            System.out.println("\n");
        }   
    }

    public void anadirObjeto(Policias policia){
        int pos_x = 0;
        int pos_y = 0;
        do{
            pos_x = (int) (Math.random() * alto);
            pos_y = (int) (Math.random() * ancho);
        }while(tablero[pos_x][pos_y] != null);
        policia.setPos_x(pos_x);
        policia.setPos_y(pos_y);
        tablero[pos_x][pos_y] = policia;
    }
    
    public void anadirObjeto(Agentes agente){
        int pos_x = 0;
        int pos_y = 0;
        do{
            pos_x = (int) (Math.random() * alto);
            pos_y = (int) (Math.random() * ancho);
        }while(tablero[pos_x][pos_y] != null);
        agente.setPos_x(pos_x);
        agente.setPos_y(pos_y);
        tablero[pos_x][pos_y] = agente;
    }
    
    public int getAlto() {
        return alto;
    }

    public int getAncho() {
        return ancho;
    }

    public Object[][] getTab() {
        return tablero;
    }

}
